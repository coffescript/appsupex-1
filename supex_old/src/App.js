import React from 'react';
import logo from './logo.svg';
import './App.css';
import Menu from './components/menu';
import Header from './components/header';
import Categories from './components/categories';
import Banner from './components/banner';
import Products from './components/products';
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="App">
  <Header/>
    <Menu/>
    <Categories/>
      <Banner/>
      <Products/>
    </div>
  );
}

export default App;
