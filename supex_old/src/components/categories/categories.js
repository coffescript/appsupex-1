import React from 'react';
import { Container,Row,Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars,faSearch} from '@fortawesome/free-solid-svg-icons'
import './categories.css';

export default function Categories(){

	return (

			<div className="categories">
			<Container className="categories_container">

				<Row className="categories__img">

					<img src="https://supexcr.com/uploads/category/1571622098_ABARROTES%20Supex%20Iconos.png" width="115px"/>
					<img src="https://supexcr.com/uploads/category/1571622075_bebidas%20Supex%20iconos.png" width="115px"/>
					<img src="https://supexcr.com/uploads/category/1571622009_CONGELADOS%20Supex%20iconos.png" width="115px"/>
					



				</Row>
			</Container>

			</div>

		);
}