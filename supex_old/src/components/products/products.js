import React from 'react';
import { Container,Row,Col,Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './products.css';

export default function Products(){

	return(
			<div className="Products">
				<Container>
				<Row>
				<Col className="product">
					<img src="https://supexcr.com/timthumb.php?src=uploads/product/1555720540_Calvo%20oferta%206.png&w=263&h=264&zc=1" width="125px"/>
					<p>Atún con vegetales 230 g + en trocitos en aceite..</p>
					<h5>₵1677</h5>
					<Button>AÑADIR</Button>
					</Col>
					<Col className="product">
					<img src="https://supexcr.com/timthumb.php?src=uploads/product/1555722496_Tesoro%20del%20Mar%20trozos%20promo.png&w=263&h=264&zc=1" width="125px"/>
					<p>Atún con vegetales Calvo 142 g + 105 g Calvo</p>
					<h5>₵1351</h5>
					<Button>AÑADIR</Button>
					</Col>
					<Col className="product">
					<img src="https://supexcr.com/timthumb.php?src=uploads/product/1556924318_Coca%20Cola%20zero%202.5L.png&w=263&h=264&zc=1" width="125px"/>
					<p>Refresco gaseoso Coca...</p>
					<h5>₵1412</h5>
					<Button>AÑADIR</Button>
					</Col>
					<Col className="product">
					<img src="https://supexcr.com/timthumb.php?src=uploads/product/1570477689_Bimbo%20pan%20artesano.png&w=263&h=264&zc=1" width="125px"/>
					<p>Pan Bimbo Artesano 535 g</p>
					<h5>₵2045</h5>
					<Button>AÑADIR</Button>
					</Col>
				</Row>
				</Container>
			</div>

		);
}