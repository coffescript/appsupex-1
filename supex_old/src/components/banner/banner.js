import React from 'react';
import { Container,Row,Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircle } from '@fortawesome/free-solid-svg-icons'
import './banner.css';
export  default function Banner(){

	return(
			<div  className="banner">
			<Container>
			<Row className="banner_img">
				<img src="img/banner1.jpg" width="80%"/>

			</Row>
			<Row>
			<Col className="banner_circles">
			   <FontAwesomeIcon icon={faCircle} />
			   <FontAwesomeIcon icon={faCircle} />
			   <FontAwesomeIcon icon={faCircle} />
			   </Col>

			</Row>
			</Container>
			</div>
		);
}