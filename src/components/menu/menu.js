import React from 'react';
import './menu.css';
import { Container,Row,Col } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTumblr, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faHome,faSearch,faShoppingCart } from '@fortawesome/free-solid-svg-icons'


export default function Menu(){

	return (
			<div className="Menu">
				<Container>
					<Row className="navMenu">
						<Col className="selected"><FontAwesomeIcon icon={faHome} /><p></p></Col>
					{/*<Col className="cart"><FontAwesomeIcon icon={faShoppingCart} /><p></p></Col>*/}
					<Col className="Cart_img"><img src="img/carrito.png" width="50px"/></Col>
    					<Col><FontAwesomeIcon icon={faSearch} /><p></p></Col>
    					
					</Row>
				</Container>
			</div>

		);
}