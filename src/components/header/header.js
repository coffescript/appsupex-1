import React,{useState, useEffect} from 'react';
import { Container,Row,Col,InputGroup } from 'react-bootstrap';
import RoomOutlinedIcon from '@material-ui/icons/RoomOutlined';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import './header.css';




export default function Header(){

	///const [cartOpen, setCartOpen] = useState('');

	

	return (

		<div className="Header">
			<Container>
			<Row className="Header__location" >
				<Col>
					<RoomOutlinedIcon/><span> Localización Actual</span>
				</Col>
			</Row>
			<Row>
					<img src="img/logo.png"/>
			</Row>
			<Row className="Header__search">
			<input type='text' class='form-control'/>
			<InputGroup.Prepend>
                  <InputGroup.Text id="inputGroupPrepend"><SearchOutlinedIcon/></InputGroup.Text>
                </InputGroup.Prepend>
			</Row>
		
			</Container>
		</div>
		);
}