import { useState, useEffect } from "react";

export function useFetch(url, token, options) {
  const [loading, setLoading] = useState(true);
  const [result, setResult] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    (async () => {
      try {
        console.log('El token es :' + token);
        const res = await fetch(url, {
          method: 'GET',
          headers: {
            'Authorization': 'Bearer ' + token
          }
        });
        const json = await res.json();
        setResult(json);
        setLoading(false);
      } catch (err) {
        setError(err);
        setLoading(false);
      }
    })();
  }, [options, url]);

  return { loading, result, error };
}
async function processFetch(url, token, options) {
  const res = await fetch(url, {
    method: 'GET',
    headers: {

      'Authorization': 'Bearer ' + token
    }

  });
  let json = await res.json();
  
  return json;

}

/*export  function Fetch(url,token,options)
{
       
       return processFetch(url,token,options);
     
      
}*/

export function useFetchLogin(url, options) {
  const [loading, setLoading] = useState(true);
  const [result, setResult] = useState({
    accessToken: false
  });
  const [error, setError] = useState(null);

  useEffect(() => {
    (async () => {
      try {
        var formBody = [];
        var details = {
          'email_id': 'dmejia1204@gmail.com',
          'password': 'admin1204'
        };
        for (var property in details) {
          var encodedKey = encodeURIComponent(property);
          var encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + "=" + encodedValue);
        }

        const res = await fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          body: formBody.join("&"),
        });
        const json = await res.json();
         await localStorage.setItem('token',json.accessToken);
        let data = await processFetch('https://api.supexcr.com/api/categories', json.accessToken, null);
        setResult(data);
        setLoading(false);

      } catch (err) {
        setError(err);
        setLoading(false);
      }

    })();
  }, [options, url]);



  return { loading, result, error };
}


